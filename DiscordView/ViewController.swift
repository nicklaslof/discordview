//
//  ViewController.swift
//  DiscordView
//
//  Created by Nicklas Löf on 17/12/15.
//  Copyright © 2015 Nicklas Löf. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIWebViewDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        let url = "https://discordapp.com/login"
        let requestURL = NSURL(string:url)
        let request = NSURLRequest(URL: requestURL!)
        webView.loadRequest(request)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBOutlet weak var webView: UIWebView!
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest r: NSURLRequest, navigationType nt: UIWebViewNavigationType) -> Bool {
        if nt == UIWebViewNavigationType.LinkClicked {
            UIApplication.sharedApplication().openURL(r.URL!)
            return false
        }
        return true
    }
    
    func goBack(sender:AnyObject) {
        if self.webView.canGoBack {
            self.webView.goBack()
        }
    }
}


